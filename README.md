------------
Description:
------------
This folder contains the files required for the Assignment-1 of the unit 
11523 - "Data Science Technology and Systems"

-------------------
Folder Description:
-------------------
data ==> The folder contains the ".csv" file and ".geojson" file for the data to be read.

------------------
Files Description:
------------------
u3220242_Assignment_1.ipynb  => code for the main question in the assignment - Part A and Part B.

-----------
How to run:
-----------
1. The data folder and the ".ipynb" file has to be in the same location.
2. Use Jupyter notebook IDE to run the code.
3. On successful running of the code user will be able to view the plots and the regression and classification models used in this project.

-------------
Requirements:
-------------
numpy
pandas
matplotlib
scikit-learn
seaborn
plotly
warnings
geopandas
fiona
